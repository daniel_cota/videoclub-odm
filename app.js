const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const expressJwt = require('express-jwt');
const jwtKey = "e6c48512f7ca5fbad890a24a1822a4a6";
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const actorsRouter = require('./routes/actors');
const membersRouter = require('./routes/members');
const moviesRouter = require('./routes/movies');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');

const uri = "mongodb://localhost:27017/videoClub";

mongoose.connect(uri);

const db = mongoose.connection;
const app = express();

db.on('error',()=>{
  console.log("Error en la conexión");
});

db.on('open',()=>{
  console.log("Conexión completada con exito");
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressJwt({secret:jwtKey,algorithms:['HS256']})
.unless({
  path:["/login"]
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/users', actorsRouter);
app.use('/members', membersRouter);
app.use('/movies', moviesRouter);
app.use('/copies', copiesRouter);
app.use('/bookings', bookingsRouter);

app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
