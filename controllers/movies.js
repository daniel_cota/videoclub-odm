const express = require('express');
const Movie = require('../models/movie');

function list(req, res, next) {
    Movie.find().then(objs => res.status(200).json({
       message: "Lista de peliculas en el sistema",
       obj: objs
    })).catch(err => res.status(500).json({
       message: "ERROR: No se puede acceder a las peliculas",
       obj: err
    }));
}

function index(req,res,next){   
    const id =req.params.id;
    Movie.findOne({"_id":id}).then(obj => res.status(200).json({
       message: `Pelicula con ID = ${id}`,
       obj: obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se encontró la pelicula con ID = ${id}`,
        obj: err
    }));
}
3.45
2.55

function create(req,res,next){
    const genre = req.body.genre;
    const title = req.body.title;
    const director = req.body.director;
    const actors = req.body.actors;
    const director_lastName = req.body.director_lastName;
    const actors_lastName = req.body.actors_lastName;
    const director_name = req.body.director_name;
    const actors_name = req.body.actors_name;

    let movie = new Movie({
        genre : genre,
        title : title,
        director : director,
        actors : actors,
        director_lastName : director_lastName,
        actors_lastName : actors_lastName,
        director_name : director_name,
        actors_name : actors_name
    });

    movie.save().then(obj => res.status(200).json({
        message : 'Pelicula registrada correctamente',
        obj: obj
    })).catch(err => res.status(500).json({
        message: 'ERROR: No se registró la pelicula',
        obj: err
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let genre = req.body.genre ? req.body.genre: "";
    let title = req.body.title ? req.body.title: "";
    let director = req.body.name ? req.body.director: "";
    let actors = req.body.actors ? req.body.actors: "";
    let director_lastName = req.body.director_lastName ? req.body.director_lastName: "";
    let actors_lastName = req.body.actors_lastName ? req.body.actors_lastName: "";
    let director_name = req.body.director_name ? req.body.director_name: "";
    let actors_name = req.body.actors_name ? req.body.actors_name: "";

    let member = new Object({
        _genre : genre,
        _title : title,
        _director : director,
        _actors : actors,
        _director_lastName : director_lastName,
        _actors_lastName : actors_lastName,
        _director_name : director_name,
        _actors_name : actors_name
    });

    Movie.findOneAndUpdate({"_id":id},movie).then(obj => res.status(200).json({
        message: "Pelicula remplazada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo reemplazar la pelicula con ID = ${id}`,
        obj: err
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    let genre = req.body.genre;
    let title = req.body.title;
    let director = req.body.director;
    let actors = req.body.actors;
    let director_lastName = req.body.director_lastName;
    let actors_lastName = req.body.actors_lastName;
    let director_name = req.body.director_name;
    let actors_name = req.body.actors_name;

    let movie = new Object();

    if(genre){
        movie._genre = genre;
    }
    if(title){
        movie._title = title;
    }
    if(director){
        movie._director = director;
    }
    if(actors){
        movie._actors = actors;
    }
    if(director_lastName){
        movie._director_lastName = director_lastName;
    }
    if(actors_lastName){
        movie._actors_lastName = actors_lastName;
    }
    if(director_name){
        movie._director_name = director_name;
    }
    if(actors_name){
        movie._actors_name = actors_name;
    }

    Movie.findOneAndUpdate({"_id":id},movie).then(obj => res.status(200).json({
        message: "Pelicula actualizada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo editar la pelicula con ID = ${id}`,
        obj: err
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Pelicula eliminada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo eliminar la pelicula con ID = ${id}`,
        obj: err
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}
