const express = require('express');
const Member = require('../models/member');

function list(req, res, next) {
    Member.find().then(objs => res.status(200).json({
       message: "Lista de miembros en el sistema",
       obj: objs
    })).catch(err => res.status(500).json({
       message: "ERROR: No se puede acceder a los miembros",
       obj: err
    }));
}

function index(req,res,next){   
    const id =req.params.id;
    Member.findOne({"_id":id}).then(obj => res.status(200).json({
       message: `Miembro con ID = ${id}`,
       obj: obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se encontró la miembro con ID = ${id}`,
        obj: err
    }));
}

function create(req,res,next){
    const address = req.body.address;
    const lastName = req.body.lastName;
    const name = req.body.name;
    const phone = req.body.phone;
    const status = req.body.status;
    const addess_city = req.body.addess_city;
    const addess_country = req.body.addess_country;
    const addess_number = req.body.addess_number;
    const addess_state = req.body.addess_state;
    const addess_street = req.body.addess_street;

    let member = new Member({
        address : address,
        lastName : lastName,
        name : name,
        phone : phone,
        status : status,
        addess_city : addess_city,
        addess_country : addess_country,
        addess_number : addess_number,
        addess_state : addess_state,
        addess_street : addess_street
    });

    member.save().then(obj => res.status(200).json({
        message : 'Miembro registrado correctamente',
        obj: obj
    })).catch(err => res.status(500).json({
        message: 'ERROR: No se registró al miembro',
        obj: err
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let address = req.body.address ? req.body.address: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let name = req.body.name ? req.body.name: "";
    let phone = req.body.phone ? req.body.phone: "";
    let status = req.body.status ? req.body.status: "";
    let addess_city = req.body.addess_city ? req.body.addess_city: "";
    let addess_country = req.body.addess_country ? req.body.addess_country: "";
    let addess_number = req.body.addess_number ? req.body.addess_number: "";
    let addess_state = req.body.addess_state ? req.body.addess_state: "";
    let addess_street = req.body.addess_street ? req.body.addess_street: "";

    let member = new Object({
        _address : address,
        _lastName : lastName,
        _name : name,
        _phone : phone,
        _status : status,
        _addess_city : addess_city,
        _addess_country : addess_country,
        _addess_number : addess_number,
        _addess_state : addess_state,
        _addess_street : addess_street
    });

    Member.findOneAndUpdate({"_id":id},member).then(obj => res.status(200).json({
        message: "Miembro remplazado correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo reemplazar al miembro con ID = ${id}`,
        obj: err
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    let address = req.body.address;
    let lastName = req.body.lastName;
    let name = req.body.name;
    let phone = req.body.phone;
    let status = req.body.status;
    let addess_city = req.body.addess_city;
    let addess_country = req.body.addess_country;
    let addess_number = req.body.addess_number;
    let addess_state = req.body.addess_state;
    let addess_street = req.body.addess_street;

    let member = new Object();

    if(address){
        member._address = address;
    }
    if(lastName){
        member._lastName = lastName;
    }
    if(name){
        member._name = name;
    }
    if(phone){
        member._phone = phone;
    }
    if(status){
        member._status = status;
    }
    if(addess_city){
        member._addess_city = addess_city;
    }
    if(addess_country){
        member._addess_country = addess_country;
    }
    if(addess_number){
        member._addess_number = addess_number;
    }
    if(addess_state){
        member._addess_state = addess_state;
    }
    if(addess_street){
        member._addess_street = addess_street;
    }

    Member.findOneAndUpdate({"_id":id},member).then(obj => res.status(200).json({
        message: "Miembro actualizado correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo editar al miembro con ID = ${id}`,
        obj: err
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Miembro eliminado correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo eliminar al miembro con ID = ${id}`,
        obj: err
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}
