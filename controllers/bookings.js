const express = require('express');
const Booking = require('../models/booking');

function list(req, res, next) {
    Booking.find().then(objs => res.status(200).json({
       message: "Lista de reservas en el sistema",
       obj: objs
    })).catch(err => res.status(500).json({
       message: "ERROR: No se puede acceder a las reservas",
       obj: err
    }));
}

function index(req,res,next){   
    const id =req.params.id;
    Booking.findOne({"_id":id}).then(obj => res.status(200).json({
       message: `Reserva con ID = ${id}`,
       obj: obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se encontró la Reserva con ID = ${id}`,
        obj: err
    }));
}

function create(req,res,next){
    const copy = req.body.copy;
    const member = req.body.member;
    const date = req.body.date;
    let booking = new Booking({
        copy: copy,
        member:member,
        date:date
    });

    booking.save().then(obj => res.status(200).json({
        message : 'Reserva registrada correctamente',
        obj: obj
    })).catch(err => res.status(500).json({
        message: 'ERROR: No se registró la reserva',
        obj: err
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let copy = req.body.copy ? req.body.copy: "";
    let member = req.body.member ? req.body.member: "";
    let date = req.body.date ? req.body.date: "";

    let booking = new Object({
        _copy: copy,
        _member : member,
        _date : date
    });

    Booking.findOneAndUpdate({"_id":id},booking).then(obj => res.status(200).json({
        message: "Reserva remplazada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo reemplazar la reserva con ID = ${id}`,
        obj: err
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    let copy = req.body.copy;
    let member = req.body.member;
    let date = req.body.date;

    let booking = new Object();

    if(copy){
        booking._copy = copy;
    }
    if(member){
        booking._member = member;
    }
    if(date){
        booking._date = date;
    }

    Booking.findOneAndUpdate({"_id":id},booking).then(obj => res.status(200).json({
        message: "Reserva actualizada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo editar la reserva con ID = ${id}`,
        obj: err
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Reserva eliminada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo eliminar la reserva con ID = ${id}`,
        obj: err
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}
