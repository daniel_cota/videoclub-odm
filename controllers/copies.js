const express = require('express');
const Copy = require('../models/copy');

function list(req, res, next) {
    Copy.find().then(objs => res.status(200).json({
       message: "Lista de copias en el sistema",
       obj: objs
    })).catch(err => res.status(500).json({
       message: "ERROR: No se puede acceder a las copias",
       obj: err
    }));
}

function index(req,res,next){   
    const id =req.params.id;
    Copy.findOne({"_id":id}).then(obj => res.status(200).json({
       message: `Copia con ID = ${id}`,
       obj: obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se encontró la Copia con ID = ${id}`,
        obj: err
    }));
}

function create(req,res,next){
    const format = req.body.format;
    const movie = req.body.movie;
    const number = req.body.number;
    const status = req.body.status;

    let copy = new Copy({
        format: format,
        movie:movie,
        number:number,
        status:status
    });

    copy.save().then(obj => res.status(200).json({
        message : 'Copia registrada correctamente',
        obj: obj
    })).catch(err => res.status(500).json({
        message: 'ERROR: No se registró la copia',
        obj: err
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let format = req.body.format ? req.body.format: "";
    let movie = req.body.movie ? req.body.movie: "";
    let number = req.body.number ? req.body.number: "";
    let status = req.body.status ? req.body.status: "";

    let copy = new Object({
        _format : format,
        _movie : movie,
        _number : number,
        _status : status
    });

    Copy.findOneAndUpdate({"_id":id},copy).then(obj => res.status(200).json({
        message: "Copia remplazada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo reemplazar la copia con ID = ${id}`,
        obj: err
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    let format = req.body.format;
    let movie = req.body.movie;
    let number = req.body.number;
    let status = req.body.status;

    let copy = new Object();

    if(copy){
        copie._format = format;
    }
    if(member){
        copie._movie = movie;
    }
    if(date){
        copie._number = number;
    }
    if(date){
        copie._status = status;
    }

    Copie.findOneAndUpdate({"_id":id},copie).then(obj => res.status(200).json({
        message: "Copia actualizada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo editar la copia con ID = ${id}`,
        obj: err
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Copy.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Copia eliminada correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo eliminar la copia con ID = ${id}`,
        obj: err
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}
