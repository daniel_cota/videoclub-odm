const express = require('express');
const Actor = require('../models/actor');

function list(req, res, next) {
    Actor.find().then(objs => res.status(200).json({
        message: "Lista de actores en el sistema",
        obj: objs
    })).catch(err => res.status(500).json({
        message: "ERROR: No se puede acceder a los actores",
        obj: err
    }));
}

function index(req,res,next){   
   const id = req.params.id;
    Actor.findOne({"_id":id}).then(obj => res.status(200).json({
        message: `Actor con ID = ${id}`,
        obj: obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se encontró al actor con ID = ${id}`,
        obj: err
    }));
}

function create(req,res,next){
    const name = req.body.name;
    const lastName = req.body.lastName;
    let actor = new Actor({
        name: name,
        lastName:lastName
    });

    actor.save().then(obj => res.status(200).json({
        message : 'Actor registradó correctamente',
        obj: obj
    })).catch(err => res.status(500).json({
        message: 'ERROR: No se registró al actor',
        obj: err
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";

    let actor = new Object({
        _name: name,
        _lastName : lastName
    });

    Actor.findOneAndUpdate({"_id":id},actor).then(obj => res.status(200).json({
        message: "Actor remplazado correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo reemplazar al actor con ID = ${id}`,
        obj: err
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let actor = new Object();

    if(name){
        actor._name = name;
    }

    if(lastName){
        actor._lastName = lastName;
    }

    Actor.findOneAndUpdate({"_id":id},actor).then(obj => res.status(200).json({
        message: "Actor actualizado correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo editar al actor con ID = ${id}`,
        obj: err
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Actor.remove({"_id":id}).then(obj => res.status(200).json({
        message: "Actor eliminado correctamente",
        obj:obj
    })).catch(err => res.status(500).json({
        message: `ERROR: No se pudo eliminar el actor con ID = ${id}`,
        obj: err
    }));
}

module.exports = {
    list, index,create,edit,replace,destroy
}
