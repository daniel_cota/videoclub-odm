# Tarea | Mi primera API con express + ODM 
# Práctica | Implementar un sistema de autenticación.

Tarea para implementar una api con express y odm 
Practica para implementar un sistema de autenticaicón en esa api

## Instrucciones 📋

### Tarea
Vamos a construir todos los servicios web relacionados con el ejemplo del video club, basado en el esquema cree para cada modelo todo su CRUD. Una vez terminado suba su proyecto a gitlab en una rama diferente a master. Suba la liga del proyecto una vez terminado.
![alt text](https://lh3.googleusercontent.com/NynPswLIqc5C6v9r1L_8GzHksLhnVdl0u4ojRMQkeF0bExiHN1qxfCfU_CzpjtL_tMZhx_UK9HF2k-qDgXBti_Rgcth_nzGbamWskGERY5qHxYVQPf5ZONIvfVB_OM-wKyPbZq2jTEzAB03Mk8CVNaWl2FTFND4xOUXNzW3lNPACeM8wEQ9jYX-pGIf6kfPqAMviwhaFQUviWBNOzIm_istsBkrLlV_aCX5ztSIh_XVJak8GJYH6dBwyqPd_55sLEC9GHvgUOwEn6thx0NgMAZtv8cFIZdnUlJrMSmzCmXCCrWyaqNwiyQFVd22JJFPfRI3h2vkipV_NX2YDrcdBJl1ZCtfTU_Grl-NYkb8jLEN0vEPn-0_EQBhp3HpHKItzjAP-xetjLKIX2AwFDuazSHgt8Gcq0g0vWgVlQczMAfQ73npQJv-uhGuFPetaVmcOmznW-9RNj42wuW4PLeCdFeCllkb75kHk7h7GmasRlcaAzTxlzMTuAUNEOUFu6D2DsBKwOFARxkl2V7pOkLfwWGpFMkkNSofR2RaxUxZZQMLTDgqKvQo9h12EgRCzYcS7WRRpvstGbQ2_K29vogk65nwKLH6R4fQktAaxr0ge_bbtX7BIBS1o-9x2j0C9fUn2rh9KBqrGIKEQrQSrNqeKjTMHd6YEjv8mF1RCHyuOvub77tBXLnBFfhgT2fTLhpIHg3vRoCixURDWGpFNsJqo6w=w328-h253-no?authuser=0)

### Practica
Vamos a implementar un sistema de autenticación utilizando JWT y la encriptación simétrica.

1. Cree un sistema para poder almacenar la contraseña, el usuario y el salt key.

2. Implemente el cifrado adecuado para cifrar la contraseña.

3. implemente el sistema por tokens y proteja sus rutas.

4. Implemente una ruta de login.

Termine los detalles de su implementación y suba sus cambios a gitlab, entregue la url de su proyecto como entregable.

## Pre-requisitos 🛠️

Clonar el repositorio

## Pasos 🔧
```
npm install
```
```
npm run dev
```

## Autor

- Daniel Alberto Cota Ochoa     ***329701***
    - [Gitlab](https://gitlab.com/daniel_cota)
